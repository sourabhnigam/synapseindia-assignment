const productsData = [
    {
        id: 1,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '120$'
    },
    {
        id: 2,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '10$'
    },
    {
        id: 10,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '130$'
    },
    {
        id: 3,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '10$'
    },
    {
        id: 4,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '10$'
    },
    {
        id: 7,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '140$'
    },
    {
        id: 5,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '150$'
    },
    {
        id: 6,
        product_title: "Planet Print Marled Hoodie\n",
        product_details: "Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
        image: "http://localhost:3000/assets/sample.jpeg",
        price: '160$'
    }
];

const  products = module.exports = productsData;
