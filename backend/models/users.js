const mongodb		=		require("mongoose"); // I usesd const because values are not going to change in future.
// Create Schema of Users 
const userSchema	=		mongodb.Schema({
				firstName : {
					type 		: String,
					require 	: true
				},
				lastName : {
					type 		: String,
					require 	: true
				},
				email : {
					type 		: String,
					require 	: true
				},
				password : {
					type 		: String,
					require 	: true
				},
				status : {
					type 		: Number,
					require 	: true
				},
				emailConfirmationCode : {
					type 		: String,
					require 	: true
				}
});
// Export Schema So we can used it

const user = module.exports = mongodb.model('user',userSchema);
