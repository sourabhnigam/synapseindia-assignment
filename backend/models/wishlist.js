const mongodb		=		require("mongoose"); // I usesd const because values are not going to change in future.
// Create Schema of Users 
const wishlistSchema	=		mongodb.Schema({
				userId : {
					type 		: String,
					require 	: true
				},
				productId : {
					type 		: Number,
					require 	: true
				}
});
// Export Schema So we can used it

const wishlist = module.exports = mongodb.model('wishlist',wishlistSchema);
