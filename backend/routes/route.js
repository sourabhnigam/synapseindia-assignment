// I am using const because value are not going to change 
const express	=	require("express");
const route		=	express.Router();
// Get users schema from users model
const user      =   require("../models/users");
const wishlist  =   require("../models/wishlist");
const products  =   require("../models/products");

const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;

const accessToken = ''; // Add your  gmail access token

const smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        type: "OAuth2",
        user: "", // Add your  gmail id
        clientId: "", // Create app and add clientId
        clientSecret: "",  // Get client secret and add
        refreshToken: "", // generate refresh token,  https://developers.google.com/oauthplayground
        accessToken: accessToken
    }
});

// I am useing arrow functions as a  callback functions.
// create Authentication Route for login users and send login token
route.post("/authenticate",(req,res,next)=>{
    user.findOne({email:req.body.username,password : req.body.password},(err,obj)=>{
        if(obj===null){
            res.json({status:0});
        }else if(obj.status==0){
            res.json({status:2});
        }else{
            res.json(obj);
        }
    });
});

// User registration   api.
route.post("/register",(req,res,next)=>{
	// creating user object and pass value in user constructor
	let newUser= new user({
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		password : req.body.password,
		status : req.body.status,
		emailConfirmationCode : req.body.emailToken,
	});
	newUser.save((err,user)=>{
		if(err){
			res.json({status: 0 , msg : "registered failed please try again!"});
		}else{
			// varify email address
            const mailOptions = {
                from: "sourabhnigam19@gmail.com",
                to: req.body.email,
                subject: "Email verify : Assigment Test",
                generateTextFromHTML: true,
                html: "<b>Hello </b>"+req.body.firstName+"<br>You are successfully registered, please click on link and verify your email address<br><a href='http://localhost:4200/verify/"+req.body.emailToken+"'>Verify</a><br>Thanks & Reagard <br> Assignment "
            };
            smtpTransport.sendMail(mailOptions, (error, response) => {
                error ? console.log(error) : console.log(response);
                smtpTransport.close();
            });
            res.json({status: 1 , msg : "Registration Successfully done"});
		}

	})
});
// Get users list this id for testing

route.get("/users",(req,res,nex)=>{
		user.find(function(err,users){
				res.json(users);
		})
})
// Email Validation Api email already exist :

route.get("/email",(req,res,next)=>{
	user.findOne({email:req.query.email},(err,obj)=>{
		if(obj===null){
			res.json({status:0});	
		}else{
            res.json({status:1});
		}
	});
});
// Email verification :
route.get("/emailverfy",(req,res,next)=>{
    user.findOne({emailConfirmationCode:req.query.code},(err,obj)=>{
        if(obj===null){
            res.json({status:0});
        }else{
            console.log(obj);
            let id = { _id: obj._id };
            let updatestatus = { $set: {status: 1 } };
            user.updateOne(id,updatestatus,(err,r)=>{
                res.json({status:1});
            });
        }
    });
});
// Product List Api
route.get("/productlist",(req,res,next)=>{
    const   p = products;
    res.send(p);
});

// Product Derails Api 

route.get("/product:id",(req,res,next)=>{
	var user	=	{name:"sourabh",lastname:"nigam",token:"xyzing"};
	res.send(user);
});


route.post("/addwishlist",(req,res,next)=>{
    console.log(req);
    let newWishlist= new wishlist({
        userId : req.body.userId,
        productId : req.body.productId,
    });
    newWishlist.save((err,wishlist)=>{
        if(err){
            res.json({status: 0 , msg : "product not added in wish list!"});
        }else{
            res.json({status: 1 , msg : "product added in wishlist!"});
        }

    })
});

route.get("/getwishlist",(req,res,next)=>{
    wishlist.find(function(err,wishlist){
        res.json(wishlist);
    })
})
route.get("/getwishlist/:userId",(req,res,next)=>{
    wishlist.find({ userId: req.params.userId },function(err,wishlist){
        let wishlisproducts = [];

        for (let i=0 ; i < wishlist.length;i++){
                for (let j=0 ; j < products.length;j++) {
                    if (products[j].id == wishlist[i].productId) {
                        let p = products[j];
                        // add reference of document id for future use
                        p._id=wishlist[i]._id;
                        wishlisproducts.push(p);
                    }
                }
        }
        res.json(wishlisproducts);
    })
})

route.get("/checkwishlist/:userId/:productId",(req,res,next)=>{
    console.log(req);
    wishlist.find({ userId: req.params.userId,productId: req.params.productId },function(err,wishlist){
        if(wishlist.length>0){
            res.json({status: 1 });
        }else{
            res.json({status: 0 });
        }

    })
})

route.get("/deletewishlist/:Id",(req,res,next)=>{
    wishlist.deleteOne({_id: req.params.Id},function (err,wishlist) {
        if(err) {
            throw err;
        }
        res.json(wishlist);
    })
})
module.exports	= route;