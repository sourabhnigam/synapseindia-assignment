// Inport Modules 
var express		=	require("express");
var mongoose	=	require("mongoose");
var	bodyParser	=	require("body-parser");
var cors		=	require("cors");
var path		=	require("path");	// Default Node module package don't need to install.

var routes		=	require("./routes/route");
var app			= 	 express();
var port 		=	3000;
var url			=	"mongodb://localhost:27017/assignmentDb";


mongoose.connect(url,(err,data)=>{
	if(err){
			console.log("not connected");	
	}else{
		console.log("Momgo Db connected succefully");	
	}
})
// Use crose for cross browsers
app.use(cors());

// Use Bodyparser 

app.use(bodyParser.json());

// Use route
app.use("/api",routes);

app.get("/",(req,res)=>{
	res.send("Server is runnging");
})

app.listen(port,()=>{
   console.log("Server is runngin at port :"+port);
});