import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AuthenticationService]
})
export class HeaderComponent implements OnInit {
  islogin	= 	false;
  constructor(
	  private route: ActivatedRoute,
	  private router: Router,
	  private authenticationService : AuthenticationService) { }

  ngOnInit() {
  	
  }

  isLoggedIn(){
  	if(localStorage.getItem("currentUser")){
  		return true;
  	}
  }
  logout(){
  		this.authenticationService.dologout();
  }

}
