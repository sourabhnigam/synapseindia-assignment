import { Component, OnInit } from '@angular/core';
import {Route, Router} from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ ProductService ]
})
export class ProductsComponent implements OnInit {
  products: any;
  private added: boolean;
  private checkw: {Object};
  private wishlistProdct: any;
  isWishlist = true;
  isloggedin = false;
  pids = [];
  constructor(
    private productservice: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.productservice.getProduct()
      .subscribe(data => {
        if ( data !== null) {
          this.products = data;
          // Check user wish list products
          const currentuser = JSON.parse(localStorage.getItem('currentUser'));
          if (currentuser !== null) {
            const userId = currentuser._id;
            this.productservice.getWishlist(userId)
              .subscribe(wishlist => {
                this.wishlistProdct = wishlist;
                for (let i = 0 ; i < this.wishlistProdct.length ; i++) {
                  // console.log(this.wishlistProdct[i].id);
                  this.pids.push(this.wishlistProdct[i].id);
                }
                for (let i = 0 ; i < this.products.length ; i++) {
                  if (this.pids.indexOf(this.products[i].id) === -1) {
                    this.products[i].isWish = false;
                  } else {
                    this.products[i].isWish = true;
                  }
                }
                console.log(this.products);
                console.log(this.pids);
              }, error => {
                console.log(error);
              });
          }
        }
      }, error => {
        return error;
      });
  }
  addWishlist(id) {
    const prooductId = id;
    const currentuser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentuser !== null) {
      const userid = currentuser._id;
      this.productservice.addWishlist(userid, prooductId)
        .subscribe(wishlist => {
          this.added = true;
          alert('Product Added in whishlist');
          this.router.navigate(['/product']);
        }, error => {
          console.log(error);
        });
    } else {
      this.isloggedin = true;
      window.scroll(0, 0);
    }
  }
}
