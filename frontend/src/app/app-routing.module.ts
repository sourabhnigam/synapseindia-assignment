import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RagisterComponent } from './ragister/ragister.component';
import { VerfyComponent } from './verfy/verfy.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { AuthguardGuard } from "./auth/authguard.guard";
import { WishlistComponent } from './wishlist/wishlist.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RagisterComponent },
  { path: 'verify/:code', component: VerfyComponent },
  { path: 'product', component: ProductsComponent},
  { path: 'wishlist', component: WishlistComponent, canActivate: [AuthguardGuard] },
  { path: 'product/:id', component: ProductdetailsComponent },
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [
     RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
