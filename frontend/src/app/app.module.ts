import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UiModule } from './ui/ui.module';
import {AuthenticationService } from './services/authentication.service';
import {UsersService } from './services/users.service';
import { LoginComponent } from './login/login.component';
import { RagisterComponent } from './ragister/ragister.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AppRoutingModule } from './/app-routing.module';
import { ConfirmValidatorDirective } from './shared/confirm-validator.directive';
import { VerfyComponent } from './verfy/verfy.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { WishlistComponent } from './wishlist/wishlist.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RagisterComponent,
    NavigationComponent,
    ConfirmValidatorDirective,
    VerfyComponent,
    HomeComponent,
    ProductsComponent,
    ProductdetailsComponent,
    WishlistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    UiModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [AuthenticationService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
