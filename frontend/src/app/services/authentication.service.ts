import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    dologin(username: string, password: string | Int32Array) {
        return this.http.post<any>('http://localhost:3000/api/authenticate', { username: username, password: password })
            .map(user => {
                // login successful if there's a token in the response
                if (user && user._id) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            });
    }

    dologout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
