import { TestBed, inject } from '@angular/core/testing';

import { VerfyService } from './verfy.service';

describe('VerfyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerfyService]
    });
  });

  it('should be created', inject([VerfyService], (service: VerfyService) => {
    expect(service).toBeTruthy();
  }));
});
