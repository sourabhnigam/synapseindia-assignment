import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  getProduct() {
    return this.http.get('http://localhost:3000/api/productlist')
    .map(product => {
       return product;
    });
  }
  getProductDetails(id: string) {
    const params = new HttpParams().set('id', id );
      return this.http.get<any>('http://localhost:3000/api/productlist', {params: params} )
        .map(productDetails => {
            return productDetails;
        });
  }
  addWishlist(userId: string, productId: number) {
    return this.http.post<any>('http://localhost:3000/api/addwishlist', { userId: userId, productId: productId })
      .map(wishlist => {
        return wishlist;
      });
  }
  getWishlist(userId: string) {
    return this.http.get<any>('http://localhost:3000/api/getwishlist/' + userId)
      .map(product => {
        return product;
      });
  }
  checkWishlist(userId: string, producctId: string) {
    return this.http.get<any>('http://localhost:3000/api/getwishlist/' + userId + '/' + producctId )
      .map(wp => {
        return wp;
      });
  }

  deleteWishlist(id: string) {
    return this.http.get('http://localhost:3000/api/deletewishlist/' + id)
      .map(product => {
        return product;
      });
  }
}
