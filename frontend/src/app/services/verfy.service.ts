import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Injectable()
export class VerfyService {

  constructor(private http: HttpClient) { }
  verfyEmail(code: string){
    let params = new HttpParams().set('code', code);
    return this.http.get<any>('http://localhost:3000/api/emailverfy',{params:params})
      .map(user=>{
        return user;
      });
  }
}
