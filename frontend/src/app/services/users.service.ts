import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {
  }
  // Call Register api
  doRegister(firstName: string, lastName: string, email: string, password: string | Int32Array, status: number, emailToken: string) {
    return this.http.post<any>('http://localhost:3000/api/register', {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      status: status,
      emailToken: emailToken
    }).map(user => {
        return user;
    });
  }

  checkEmail(email: string){
    let params = new HttpParams().set('email', email);
    return this.http.get<any>('http://localhost:3000/api/email',{params:params})
      .map(user => {
        return user;
      });
  }
}

