import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate( router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if ( localStorage.getItem('currentUser') ) {
      // User logged in
      return true;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return true;
  }
}
