import { Directive ,Input} from '@angular/core';
import {Validator, AbstractControl, ValidationErrors, NG_VALIDATORS} from '@angular/forms';
import {Subscription} from "rxjs";
@Directive({
  selector: '[compare]',
  providers:[{provide : NG_VALIDATORS, useExisting: ConfirmValidatorDirective, multi:true }]
})
export class ConfirmValidatorDirective implements Validator {
   @Input('compare') controlNameToCompare: string;
  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {
      const controleToComapre = control.root.get(this.controlNameToCompare);
      if(controleToComapre){
        const subscription: Subscription = controleToComapre.valueChanges.subscribe(()=>{
          control.updateValueAndValidity();
          subscription.unsubscribe();
        });
        return controleToComapre && controleToComapre.value !== control.value ? {'compare' : true} : null;
      }
  }
}
