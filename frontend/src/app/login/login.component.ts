import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService } from '../services/authentication.service';
import {Md5} from "ts-md5";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers :[AuthenticationService]
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  md5 = new Md5();
  mdfpassword: string | Int32Array;
  private error: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    console.log(localStorage.getItem('currentUser'));
    if (localStorage.getItem('currentUser')) {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      this.router.navigate([this.returnUrl]);
    }
    // this.authenticationService.dologout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  dologin() {
    this.loading = true;
    this.mdfpassword = this.md5.appendStr(this.model.password).end();
        this.authenticationService.dologin(this.model.username, this.mdfpassword)
            .subscribe(
                data => {
                  if (data.status === 2) {
                    this.error = '<div class="alert alert-danger">' +
                      '<strong>Error!</strong> ' +
                      'Email not verified yet please verify your email address' +
                      '</div>';
                    this.loading = false;
                  } else if (data.status === 0) {
                    this.error = '<div class="alert alert-danger">' +
                      '<strong>Error!</strong> ' +
                      'Bad credentials please try again' +
                      '</div>';
                    this.loading = false;
                  } else {
                    this.router.navigate([this.returnUrl]);
                  }
                },
                error => {
                    this.loading = false;
                });
  }

}
