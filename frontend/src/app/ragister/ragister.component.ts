import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from "../services/users.service";
import { Md5 } from 'ts-md5/dist/md5';
import {stringDistance} from "codelyzer/util/utils";

@Component({
  selector: 'app-ragister',
  templateUrl: './ragister.component.html',
  styleUrls: ['./ragister.component.css'],
  providers:[UsersService]
})
export class RagisterComponent implements OnInit {
  model: any = {};
  registerForm: FormGroup;
  submitted = false;
  loading = false;
  email = false;
  md5 = new Md5();
  mdfpassword: string | Int32Array;
  isDone= false;
  constructor(private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private router: Router,
  private usersService: UsersService) { }


  randomString = this.makeid();

  get f() { return this.registerForm.controls; }

  ngOnInit() {
  	this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
            confirmPassword : ['',[Validators.required]]
        })
  }
  onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.loading=true;
        this.mdfpassword = this.md5.appendStr(this.registerForm.value.password).end();
        // check mail already exist
        this.usersService.checkEmail(this.registerForm.value.email)
          .subscribe(data=>{
            if(data.status==1){
              this.email = true;
              this.submitted = false;
              this.loading=false;
              return;
            }else{
              // call Registaration service
              this.usersService.doRegister(
                this.registerForm.value.firstName,
                this.registerForm.value.lastName,
                this.registerForm.value.email,
                this.mdfpassword,
                0,
                this.randomString
              ).subscribe(data=>{
                if(data.status==1){
                  this.isDone =true;
                }
              },error =>{
                console.log(error);
              });
            }
          },error=>{
            console.log("error");
          });
    }

  makeid() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

}
