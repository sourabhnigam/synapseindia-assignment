import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import {count} from 'rxjs/operators';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css'],
  providers: [ProductService]
})
export class WishlistComponent implements OnInit {
  isWishlist = false;
  wishlistProdct: Object;
  constructor(
    private productwishlist: ProductService
  ) { }

  ngOnInit() {
    const currentuser = JSON.parse(localStorage.getItem('currentUser'));
    const userId = currentuser._id;
    this.productwishlist.getWishlist(userId)
      .subscribe(wishlist => {
        this.wishlistProdct = wishlist;
        console.log(wishlist.length);
          if (wishlist.length === 0) {
            this.isWishlist = false;
          } else {
            this.isWishlist = true;
          }
      }, error => {
        console.log(error);
      });
  }
  removeWishlist(id: string) {
    const prooductId = id;
    this.productwishlist.deleteWishlist(id)
      .subscribe(wishlist => {
        this.refrashWishlist();
        // console.log(this.wishlistProdct);
        // this.refrashWishlist();
        // if (this.wishlist.length === 0) {
        //   this.isWishlist = true;
        // }
      }, error => {
        console.log(error);
      });
  }
  refrashWishlist() {
    const currentuser = JSON.parse(localStorage.getItem('currentUser'));
    const userId = currentuser._id;
    this.productwishlist.getWishlist(userId)
      .subscribe(wishlist => {
        this.wishlistProdct = wishlist;
        if (wishlist.length === 0) {
          this.isWishlist = false;
        }else {
          this.isWishlist = true;
        }
      }, error => {
        console.log(error);
      });
  }
}
