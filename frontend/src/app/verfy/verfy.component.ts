import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute , ParamMap} from '@angular/router';
import { VerfyService } from '../services/verfy.service';

@Component({
  selector: 'app-verfy',
  templateUrl: './verfy.component.html',
  styleUrls: ['./verfy.component.css'],
  providers: [VerfyService]
})
export class VerfyComponent implements OnInit {
  verfystring:string="";
  constructor(private route: ActivatedRoute,
              private router: Router,
              private verfyservice:VerfyService
  ) { }

  ngOnInit() {
      this.verfyservice.verfyEmail(this.route.snapshot.params['code'])
        .subscribe(data=>{
          if(data.status==1){
            this.verfystring='<div class="alert alert-success">' +
              '  <strong>Success!</strong> Verification completed successfully. <a href="/login">Login</a>' +
              '</div>';
          }else{
            this.verfystring='<div class="alert alert-danger">' +
              '  <strong>Error!</strong> Verification not successfully try again.' +
              '</div>';
          }
        })
  }
}
